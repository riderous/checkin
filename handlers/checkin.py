
import motor
import time

from datetime import datetime
from bson.objectid import ObjectId
from bson.json_util import dumps, loads
from tornado import web, gen, httpclient

POST_PROPERTIES = {'long', 'lat', 'accuracy', 'course', 'speed', 'measured', 'annotation'}

GEOCODING_URL_TEMPLATE = 'http://maps.googleapis.com/maps/api/geocode/json?latlng={},{}&sensor=false'


class CheckinHandler(web.RequestHandler):

    def initialize(self):
        self.db = self.settings['db']
        self.req_body_dict = None
        self.http_client = httpclient.AsyncHTTPClient()

    def prepare(self):
        if self.request.body:
            self.req_body_dict = loads(self.request.body)

    def finish_response(self, model, message):
        self.set_header('Content-Type', 'application/json')

        if model and '_id' in model:
            model['_id'] = str(model['_id'])

        self.write(dumps({
            'model': model,
            'status': 'ok',
            'message': message,
        }))

    def write_error(self, status_code, **kwargs):
        self.set_header('Content-Type', 'application/json')

        self.write(dumps({
            'model': None,
            'status': 'ERROR',
            'message': str(kwargs['exc_info'][1])
        }))


class CheckinGETHandler(CheckinHandler):

    @web.asynchronous
    @gen.coroutine
    def get(self, checkin_id):
        model = yield motor.Op(self.db.checkins.find_one, {'_id': ObjectId(checkin_id)})
        if not model:
            raise web.HTTPError(404, 'No such checkin: {}'.format(checkin_id))

        self.set_status(200)
        self.finish_response(model, 'Successfully received')


class CheckinPOSTHandler(CheckinHandler):

    @web.asynchronous
    @gen.coroutine
    def post(self):
        self._validate_properties()

        model = {
            'address': (yield gen.Task(self._get_address, self.req_body_dict['lat'], self.req_body_dict['long'])),
            'date': time.mktime(datetime.utcnow().timetuple())
        }
        model.update(self.req_body_dict)
        model['_id'] = yield motor.Op(self.db.checkins.insert, model)

        self.set_status(201)
        self.finish_response(model, 'Successfully created')

    def _validate_properties(self):
        if set(self.req_body_dict.keys()) != POST_PROPERTIES:
            raise web.HTTPError(400, 'List of properties should be: {}'.format(list(POST_PROPERTIES)))

    @gen.engine
    def _get_address(self, latitude, longitude, callback):
        response = yield gen.Task(self.http_client.fetch, GEOCODING_URL_TEMPLATE.format(latitude, longitude))

        if response.code != 200:
            raise web.HTTPError(500, 'Error communicating with Geocoding service: {}'.format(response.error))

        reversed_geocoding = loads(response.body)

        if reversed_geocoding['status'].lower() != 'ok':
            raise web.HTTPError(500,
                'Geocoding service responded with the {} status'.format(reversed_geocoding['status']))

        callback(reversed_geocoding['results'][0]['formatted_address'])
