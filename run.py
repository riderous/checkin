
import tornado.options

from motor import MotorClient
from tornado import web, ioloop
from tornado.options import define, options

from routes import get_routes

define('on_port', default=8001, type=int)
define('db_host', default='localhost')
define('db_port', default=27017, type=int)
define('db_name', default='checkin')
define('db_user', default='alex')
define('db_pass', default='pass')


class CheckinApp(web.Application):

    @classmethod
    def db_connect(cls):
        uri = 'mongodb://{}:{}@{}:{}/{}'
        uri_format = uri.format(options.db_user, options.db_pass, options.db_host,
                                options.db_port, options.db_name)
        return MotorClient(uri_format).open_sync()[options.db_name]


def run():
    tornado.options.parse_command_line()
    app = CheckinApp(get_routes(), db=CheckinApp.db_connect())
    app.listen(options.on_port)
    ioloop.IOLoop.instance().start()


if __name__ == '__main__':
    run()
