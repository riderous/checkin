
from handlers.checkin import CheckinGETHandler, CheckinPOSTHandler


routes = [
    (r'/checkin/([\w]+)', CheckinGETHandler),
    (r'/checkin/?', CheckinPOSTHandler),
]


def get_routes():
    return routes
